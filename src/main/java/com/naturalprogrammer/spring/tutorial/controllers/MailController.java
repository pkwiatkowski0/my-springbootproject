package com.naturalprogrammer.spring.tutorial.controllers;

import com.naturalprogrammer.spring.tutorial.mail.MailSender;
import com.naturalprogrammer.spring.tutorial.mail.MockMailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.mail.MessagingException;

/**
 * Created by patryk on 28.05.2017.
 */
@RestController
public class MailController {

    private MailSender mailSender;

    @Autowired
    public MailController(MailSender mailSender) {
        this.mailSender = mailSender;
    }

    @RequestMapping ("/mail")
    public String sendMail() throws MessagingException {
        mailSender.send("pkwiatkowski12345@gmail.com", "Ignore that test mail sent from my awesome spring boot app!", "The content blablabla");
        return "Mail has been sent";
    }

}
