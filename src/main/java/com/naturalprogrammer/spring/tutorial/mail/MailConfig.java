package com.naturalprogrammer.spring.tutorial.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.mail.javamail.JavaMailSender;

/**
 * Created by patryk on 02.06.2017.
 */
@Configuration
public class MailConfig {


    @Autowired
    private JavaMailSender javaMailSender;

    @Bean
    @ConditionalOnProperty(name = "spring.mail.host",
            havingValue = "foo",
            matchIfMissing = true)
    public MailSender mockMailSender() {
        return new MockMailSender();
    }

    @Bean
    @Primary
    @ConditionalOnProperty(name = "spring.mail.host")
    public MailSender smtpMailSender() {
        SmtpMailSender mailSender = new SmtpMailSender();
        mailSender.setJavaMailSender(javaMailSender);
        return mailSender;
    }

}
